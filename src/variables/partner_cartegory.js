module.exports = [
  {
    name: "Corporate Partners",
    partners: [1,2]
  },
  {
    name: "Academic Partners",
    partners: [3,4,5,6]
  },
  {
    name: "Investors",
    partners: [7,8,9,10,11,12,13,14]
  },
  {
    name: "Eco-system partners",
    partners: [15,16,17,18]
  },
  {
    name: "Govt./ NGO Partners",
    partners: [19,20,21,22]
  },
  {
    name: "Service Providers",
    partners: [23,24,25,26,27,28,29,30,31,32,33,34]
  }
]