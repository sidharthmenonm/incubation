module.exports = [
  {
    "id": 1,
    "image": "ey.jpg",
    "name": "Ernst & Young"
  },
  {
    "id": 2,
    "image": "bp.jpg",
    "name": "Bharath Petroleum Corporation"
  },
  {
    "id": 3,
    "image": "iimb.jpg",
    "name": "IIM,  Bangalore"
  },
  {
    "id": 4,
    "image": "iimk.jpg",
    "name": "IIM, Kozhikode"
  },
  {
    "id": 5,
    "image": "mit.jpg",
    "name": "Center for Bits and Atoms, MIT"
  },
  {
    "id": 6,
    "image": "d4i.jpg",
    "name": "Design 4 India"
  },
  {
    "id": 7,
    "image": "ma.jpg",
    "name": "Mumbai Angels"
  },
  {
    "id": 8,
    "image": "ian.jpg",
    "name": "Indian Angel Network"
  },
  {
    "id": 9,
    "image": "la.jpg",
    "name": "Lead Angels"
  },
  {
    "id": 10,
    "image": "ca.jpg",
    "name": "Chennai Angels"
  },
  {
    "id": 11,
    "image": "uiv.jpg",
    "name": "Unicorn India Ventures"
  },
  {
    "id": 12,
    "image": "ex.jpg",
    "name": "Exseed"
  },
  {
    "id": 13,
    "image": "is.jpg",
    "name": "Speciale Invest"
  },
  {
    "id": 14,
    "image": "sf.jpg",
    "name": "Sea Fund"
  },
  {
    "id": 15,
    "image": "nasscom.jpg",
    "name": "NASSCOM"
  },
  {
    "id": 16,
    "image": "iamai.jpg",
    "name": "IAMAI"
  },
  {
    "id": 17,
    "image": "zone.jpg",
    "name": "Zone Startups"
  },
  {
    "id": 18,
    "image": "headstart.jpg",
    "name": "Headstart"
  },
  {
    "id": 19,
    "image": "ksidc.jpg",
    "name": "KSIDC"
  },
  {
    "id": 20,
    "image": "di.jpg",
    "name": "Digital India"
  },
  {
    "id": 21,
    "image": "kseb.jpg",
    "name": "KSEB"
  },
  {
    "id": 22,
    "image": "rgcb.jpg",
    "name": "RGCB"
  },
  {
    "id": 23,
    "image": "google.jpg",
    "name": "Google Gloud"
  },
  {
    "id": 24,
    "image": "aws.jpg",
    "name": "Amazon Web Services"
  },
  {
    "id": 25,
    "image": "do1.jpg",
    "name": "Digital Ocean"
  },
  {
    "id": 26,
    "image": "collect.jpg",
    "name": "Collect.chat"
  },
  {
    "id": 27,
    "image": "cleartax.jpg",
    "name": "Clear Tax"
  },
  {
    "id": 28,
    "image": "razor.jpg",
    "name": "Razorpay"
  },
  {
    "id": 29,
    "image": "mt.jpg",
    "name": "M Talks"
  },
  {
    "id": 30,
    "image": "m91.jpg",
    "name": "MSG 91"
  },
  {
    "id": 31,
    "image": "hubspot.jpg",
    "name": "HubSpot"
  },
  {
    "id": 32,
    "image": "rra.jpg",
    "name": "Rakuten Rapid Api"
  },
  {
    "id": 33,
    "image": "textvox.jpg",
    "name": "TestVox"
  },
  {
    "id": 34,
    "image": "pm.jpg",
    "name": "Presentation Monk"
  }
]