import Vue from 'vue';
import partners_list from '../variables/partners';
import category_list from '../variables/partner_cartegory';

new Vue({ 
  el: '#partners-section',
  data: {
    partners: partners_list,
    categories: category_list
  },
})