import Vue from 'vue';
import portfolio_list from '../variables/portfolio';

new Vue({ 
  el: '#portfolio-section',
  data: {
    portfolios: portfolio_list,
    filter_cat: "all",
  },
  computed:{
    filtered(){
      var filtered = this.portfolios;
      var vm = this;
      if(this.filter_cat && this.filter_cat!="all"){
        filtered = filtered.filter(function(item){
          return item.category==vm.filter_cat
        })
      }
      return filtered;
    },
    categories(){
      var venue = this.portfolios.map(function(item){
        return item.category;
      })
      return [... new Set(venue)];
    }
  }
});