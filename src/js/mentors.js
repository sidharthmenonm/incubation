import Vue from 'vue';
import mentor_list from '../variables/mentors';

new Vue({ 
  el: '#mentor-section',
  data: {
    mentors: mentor_list,
  }
});